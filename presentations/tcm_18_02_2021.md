---
title: "Лекция 4. ДКА и НКА"
block: "Теоретические модели вычислений"
order: 4

---

#### Теоретические модели вычислений. 
#### Лекция 4. ДКА и НКА

---

#### Определения с прошлой лекции

$Σ$ - конечный алфавит символов.

Слово - конечная последовательность символов

Язык $L$ - множество слов.

Все возможные регулярные языки:

1. $∅$
2. $λ$ (множество, состоящее из пустого слова)
3. $a$, $\forall a ∈ Σ$
4. Язкыки, которые могут быть полученены объединением $A \cup B$, конкатенацией `$A*B$` и итерацией $A^*$ 

---

### Регулярные выражения

Мы определили, что регулярным языкам соответствуют регулряные выражения.

Регулярное выражение (англ. regular expression) над алфавитом $Σ={a_1,a_2,…,a_k}$ — способ порождения языка над $Σ$. 

Определяется рекурсивно следующим образом:

1. Для любого $i$ слово $a_i$ является регулярным выражением, задающим язык из одного слова $a_i$.
2. $\lambda$ является регулярным выражением, задающим язык из одной пустой строки, а $∅$ — пустой язык.
3. Если $\alpha_1$ и $α_2$ являются регулярными выражениями, задающими языки $L_1$ и $L_2$ соответственно, то $(α_1+α_2)$ — регулярное выражение, задающее $L_1⋃L_2$.

---

### Регулярные выражения

4. Если $α_1$ и $α_2$ являются регулярными выражениями, задающими языки $L_1$ и $L_2$ соответственно, то `$α_1α_2$` — регулярное выражение, задающее `$L_1L_2$`.
5. Если $α_1$ является регулярным выражением, задающим язык $L_1$, то `$α_1^*$` — регулярное выражение, задающее $L^*_1$.
6. Операции указаны в порядке возрастания приоритета, при этом скобки повышают приоритет аналогично арифметическим выражениям.

Какой язык задаёт выражение:

1. $a(a+b)^*$ -- ?
1. `$a^*b^*$` -- ?
1. `$www.(a+b+c+d+\cdots+z)^*.ru$` -- ?
1. `$(ab^+)^*$` -- ?

---

### Детерминированные конечные автоматы

Языки можно также задавать детерминированными конечными автоматами.

Детерминированный конечный автомат (ДКА) (англ. deterministic finite automaton (DFA)) — набор из пяти элементов $⟨Σ,Q,s∈Q,T⊂Q,δ:Q×Σ→Q⟩$, где 
* $Σ$ — алфавит (англ. alphabet)
* $Q$ — множество состояний (англ. finite set of states)
* $s$ — начальное (стартовое) состояние (англ. start state)
* $T$ — множество конечных (допускающих) состояний (англ. set of accept states)
* $δ$ — функция переходов (англ. transition function).

Функция переходов определена однозначно, поэтому ДКА.

---

### Связь между ДКА и языком

Изначально автомат находится в стартовом состоянии $s$. 

Автомат считывает символы по очереди. При считывании очередного символа $p_i$ автомат переходит в состояние $δ(q,p_i)$, где $q$ — текущее состояние автомата. 

Процесс продолжается до тех пор, пока не будет достигнут конец входного слова.

Будем говорить, что автомат допускает (англ. accept) слово, если после окончания описанного выше процесса автомат окажется в конечном состоянии.

Если в автомате нет перехода из заданного состояния по заданной букве, то будем говорить, что автомат не допускает слово.

---

### Диаграмма переходов

Диаграмма переходов — граф, вершины которого соответствуют состояниям автомата, а рёбра — переходам между состояниями.

* $◯$ — нетерминальное состояние
* $⊚$ — терминальное состояние

Стрелка $↓$ указывает на начальное состояние.

---

### Пример ДКА

![](/images/tcm_18_02_2021/Automata_Search.png)

---

### И ещё примеры ДКА

![](/images/tcm_18_02_2021/Finite_state_machine_1.png)

![](/images/tcm_18_02_2021/Finite_state_machine_2.png)

---

# Таблица переходов ДКА

Таблица переходов $T(|Q|×|Σ|)$, дающая табличное представление функции $δ$.

$M=(Q,Σ,δ,q_0,F)$, где

`$Q=\{S_1,S_2\}$`

`$Σ=\{0,1\}$`

$q_0=S_1$

$F=S_1$

δ — функция переходов, представленная таблицей:

&nbsp; | 0  | 1 |
---|----|----|
S1	| S2 | S1 |
S2 | S1 | S2 |

---

### Прямое произведение ДКА

Задача: построить ДКА, который допускает только те слова, которые принадлежат языкам $A_1$ и $A_2$, т.е. построить автомат, допускающий пересечение языков.

Прямым произведением двух ДКА $A_1=⟨Σ_1,Q_1,s_1,T_1,δ_1⟩$ и $A_2=⟨Σ_2,Q_2,s_2,T_2,δ_2⟩$ называется ДКА $A=⟨Σ,Q,s,T,δ⟩$, где:

* $Σ=Σ_1∪Σ_2$
* $Q=Q_1×Q_2$
* $s=⟨s_1,s_2⟩$
* $T=T_1×T_2$
* $δ(⟨q_1,q_2⟩,c)=⟨δ_1(q_1,c),δ_2(q_2,c)⟩$

---

### Прямое произведение ДКА

Возьмем автоматы:

`$A_1=⟨Σ=\{0,1\},Q_1=\{s_1,t_1\},s_1,T_1=\{t_1\},δ_1⟩$`

`$A_2=⟨Σ=\{0,1\},Q_2=\{s_2,q_2,t_{21},t_{22}\},s_2,T_2=\{t_{21},t_{22}\},δ2⟩$`

![](/images/tcm_18_02_2021/Multi_DKA_source.png)

---

### Прямое произведение ДКА

`$Σ=\{0,1\}$`

`$Q=\{⟨s_1,s_2⟩,⟨s_1,q_2⟩,⟨s_1,t_{21}⟩,⟨s_1,t_{22}⟩,⟨t_1,s_2⟩,⟨t_1,q_2⟩,⟨t_1,t_{21}⟩,⟨t_1,t_{21}⟩\}$`

$s=⟨s_1,s_2⟩$

`$T=\{⟨t_1,t_{21}⟩,⟨t_1,t_{22}⟩\}$`

$δ$:
* $δ(⟨s_1,s_2⟩,0)=⟨δ_1(s_1,0),δ2(s_2,0)⟩=⟨s_1,q_2⟩$
* $δ(⟨s_1,s_2⟩,1)=⟨δ_1(s_1,1),δ_2(s_2,1)⟩=⟨t_1,s_2⟩$
* $δ(⟨s_1,q_2⟩,0)=⟨δ_1(s_1,0),δ_2(q_2,0)⟩=⟨s_1,q_2⟩$
* `$δ(⟨s_1,q_2⟩,1)=⟨δ_1(s_1,1),δ_2(q_2,1)⟩=⟨t_1,t_{21}⟩$`
* $\cdots$

![](/images/tcm_18_02_2021/Multi_DKA_result.png)

---

### Объединение ДКА

Необходимо разрешать любую цепочку, удовлетворяющую первому или второму автомату. Для этого сделаем терминальными следующие вершины $T=(T_1×Q_2)∪(Q_1×T_2)$. Полученный автомат удовлетворяет нашим требованиям, так как попав в какое-либо состояние из $T_1$ или $T_2$, цепочка будет удовлетворять первому или второму автомату соответственно.

![](/images/tcm_18_02_2021/Multi_DKA_united.png)

---

### Разность ДКА

Рассмотрим автомат $\bar{M}=⟨Σ,Q,s,Q∖T,δ⟩$, то есть автомат $M$, в котором терминальные и нетерминальные состояния инвертированы, если в автомате было опущено «дьявольское состояние», его необходимо добавить и сделать терминальным. Очевидно, он допускает те и только те слова, которые не допускает автомат $M$, а значит, задаёт язык $\bar{M}$.

Заметим, что если L и M — регулярные языки, то $L∖M=L∩\bar{M}$ — так же регулярный.

Следовательно, надо построить пересечение двух автоматов, предварительно инвертировав во втором терминальные и нетерминальные состояния. Заметим, что меняется только набор терминальных вершин, следовательно в итоговой конструкции произведения ДКА сделаем терминальными следующие вершины $T=T_1×(Q_2∖T_2)$.

![](/images/tcm_18_02_2021/Multi_DKA_division.png)


---

### $\lambda$-переходы

Мы можем предложить более слабую версию ДКА, которая предполагает наличие $\lambda$-переходов. 

То есть чтобы перейти по стрелочке, отмеченной $\lambda$ не требуется переместиться по входной строке. 

Это изменяет процесс обработки автомата, однако по ДКА c $\lambda$-переходами можно построить эквивалентный ему ДКА без подобных переходов.

![](/images/tcm_18_02_2021/0+1.png)


---

### Недетерминированный конечный автомат

Недетерминированный конечный автомат (НКА) (англ. Nondeterministic finite automaton, NFA) — пятёрка $⟨Σ,Q,s∈Q,T⊂Q,δ:Q×Σ→2^Q⟩$, где 
* Σ — алфавит
* Q — множество состояний автомата
* s — начальное состояние автомата
* T — множество допускающих состояний автомата
* δ — функция переходов. 

Таким образом, единственное отличие НКА от ДКА — существование нескольких переходов по одному символу из одного состояния.

По НКА можно построить эквивалентный ему НКА без $\lambda$-переходов, по НКА без $\lambda$-переходов можно построить эквивалентный ему ДКА.


---

### Построение ДКА по регулярному выражению

Алгоритм:
1. Построение НКА
2. Удаление $\lambda$-переходов
3. Построение ДКА по НКА

---

### Построение НКА по регулярному выражению

Автоматы, допускающие языки из одной буквы, пустой язык и $\lambda$-переход.

![](/images/tcm_18_02_2021/basis.png)

---

### Построение НКА по регулярному выражению

Построение объединения, конкатенации и итерации

<img src="/images/tcm_18_02_2021/RegToAut.png" alt="drawing" height="500px"/>

---

### Пример

Требуется построить НКА, допускающий следующее выражение: $(0+1)^*1(0+1)$ 

---

### Пример

$(0+1)$ 

![](/images/tcm_18_02_2021/0+1.png)


---

### Пример

$(0+1)^*$ 

![](/images/tcm_18_02_2021/(0+1)star.png)


---

### Пример

$(0+1)^*1(0+1)$ 
![](/images/tcm_18_02_2021/(0+1)star1(0+1).png)