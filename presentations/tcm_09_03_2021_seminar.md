---
title: "Семинар 09.03.2021"
block: "Теоретические модели вычислений (семинары)"
order: 102

---

## Теоретические модели вычислений. 
## Семинар


#### 9 марта 2021 года

---

### Лемма о разрастании

Если $L$ – регулярный язык, тогда

$$\exists p \in \mathbb{N}, \ \forall s \in L,\ |s|\ge p$$ <!-- .element: class="fragment" -->

слово $s$ может быть разложено на 3 части $s=xyz$ так, 
что будут выполнены все следующие условия: <!-- .element: class="fragment" -->
1. $\forall i \in \mathbb{N}_0,\ xy^iz \in A$, <!-- .element: class="fragment" -->
2. $|y|>0$, <!-- .element: class="fragment" -->
3. $|xy|\leq p$. <!-- .element: class="fragment" -->

---

### Лемма о разрастании

То есть:
1. Для любого $L$ регулярного языка можно найти некоторое число $p$. <!-- .element: class="fragment" -->
2. Любое слово $\alpha$, длина которого больше $|p|$ можно представить в виде $\alpha = xyz$ <!-- .element: class="fragment" -->
3. Полученное представление можно накачивать увеличивая степень $y$, при этом $xy^iz \in  L$. <!-- .element: class="fragment" -->

---

### Лемма о разрастании

Это __необходимое__ условие регулярности, т.е. может быть язык, который можно так накачивать, но он не будет регулярным.

<p class="fragment">
Лемму о разрастании можно использовать для доказательства <b>нерегулярности</b> языков. 
</p>

---

### Доказательство нерегулярности

Для доказательства нерегулярности языка удобно использовать отрицание леммы о разрастании. 

Пусть $L$ — язык над алфавитом $\Sigma$.  <!-- .element: class="fragment" -->

Алгоритм: <!-- .element: class="fragment" -->
1. Фиксируем некоторе $p \in \mathbb{N}$. <!-- .element: class="fragment" -->
2. Ищем некоторое слово $\omega$ из данного языка, его длина будет не меньше $p$. <!-- .element: class="fragment" -->
3. Показываем, что при любом разбиении на три слова $x,y,z$ по лемме о накачке мы будем получать слово, не из языка $L$. <!-- .element: class="fragment" -->
4. Если это не следует из пункта (1), то показываем, что (3) верно для всех $p$. <!-- .element: class="fragment" -->

---

### Доказательство нерегулярности

Рассмотрим такой подход на примере языка из $n$ открывающих и $n$ закрывающих скобок $L = \{ (^n)^n }$.

1. Для фиксированного $p$  <!-- .element: class="fragment" -->
2. Фиксируем слово $\omega=(^p)^p$.  <!-- .element: class="fragment" -->
3. Пусть $\omega$ как-то разбили на $x, y, z$. Так как $|xy|\leqslant p$, то $y=(^b$, где $b > 0$. Для любого такого разбиения берём $i=2$ и получаем $xy^iz=(^{p+b})^n$, которое не принадлежит языку. <!-- .element: class="fragment" -->

Значит, язык $L$ нерегулярный. <!-- .element: class="fragment" -->

---

### Доказательство нерегулярности

Поскольку мы с вами доказали, что множество регулярных языков замкнуто относительно теоретико-множественных операций, то мы можем доказать нерегуялрность следующим образом.

1. Пусть $L$ -- язык, нерегулярность которого мы доказываем. <!-- .element: class="fragment" -->
2. Пусть $L_1$ -- нерегулярный язык. <!-- .element: class="fragment" -->
2. Пусть $L_2$ -- регулярный язык. <!-- .element: class="fragment" -->

Тогда нерегулярность $L$ будут доказывать следующие выражения: <!-- .element: class="fragment" -->

$$L_1 = L \land L_2 $$ <!-- .element: class="fragment" -->
$$L_1 = L \setminus L_2 $$ <!-- .element: class="fragment" -->

И другие подобные. <!-- .element: class="fragment" -->


---

### Доказательство нерегулярности

Рассмотрим язык правильных скобочных последовательностей.

Его можно пересечь с языком $(^\ast)^\ast$, тогда мы получим язык  $L = \{ (^n)^n }$, нерегулярность которого была доказана ранее. <!-- .element: class="fragment" -->

---

## Различимость

Пу $L$ – произвольный язык над $\Sigma$. 

<p class="fragment">
Два слова $x \in \Sigma^\ast$ и $y \in \Sigma^\ast$ называются 
<b>различимыми относительно языка L</b>, если существует слово
$\omega \in \Sigma^\ast$ такое, что только одно из слов 
$x\omega$ и $y\omega$ принадлежит $L$. 
</p>

$$ \exists \omega \in \Sigma^* \ (x\omega \in L \iff y\omega \notin L). $$ <!-- .element: class="fragment" -->

Обозначается $x \not\equiv_L y$. <!-- .element: class="fragment" -->

Пример: $a^2 \not\equiv_E a^4$, где в качестве $\omega$ можно
рассматривать слово $b^4$. <!-- .element: class="fragment" -->

---

### Теорема Майхилла-Нероуда

Пусть $L$ -- язык в $\Sigma$. Если существует множество слов
$S \subseteq \Sigma^\*$, обладающее следующими свойствами:
* $S$ бесконечно, <!-- .element: class="fragment" -->
* слова в $S$ попарно различимы относительно $L$; <!-- .element: class="fragment" -->

тогда $L$ -- не регулярный язык. <!-- .element: class="fragment" -->

---

### Пример

Доказать нерегулярность языка:

`$$L = \{ a^n b^n | n \in \mathbb{N}_0 \}$$` <!-- .element: class="fragment" -->

<p class="fragment">
Пусть `$S_1 = \{ a^n | n \in \mathbb{N}_0\}$`.
</p>

Он принадлежит к тому же алфавиту, что и $L$ и бесконечен.  <!-- .element: class="fragment" -->

Надо показать, что все слова этого множества попарно различимы относительно $L$. <!-- .element: class="fragment" -->

Возьмём слова $a^k$ и $a^m$, где $k, m \in \mathbb{N}_0$, $k \ne m$. <!-- .element: class="fragment" -->

Возьмём $b^m$ и добавим к этим словам. Тогда $a^kb^m \notin L$, а $a^mb^m \in L$. <!-- .element: class="fragment" -->

Следовательно, слова из $S$ попарно различимы относительно $L$ и $L$ -- не регулярный язык. <!-- .element: class="fragment" -->

---

### Задача 1

Доказать нерегулярность языка:

`$L = \{0^n\ 1^m\ 0^n | m,n >= 0\}$`


---

### Задача 2

Доказать нерегулярность языка:

`$$L = \{0^m 1^n | m \ne n \}$$`

---

### Задача 3

Доказать нерегулярность языка:


`$$L = \{ \omega | \omega \in \{0,1\}^* — \text{не палиндром} \}$$`


<!-- Возьмите его пересечение с языком, описываемым регулярным выражением 0*110* и воспользуйтесь леммой о накачке.-->

<!-- Example 2: L2 = { ww | w  {a, b }* } is nonregular.
Consider the set of strings S2 which is the same as S1 of Example 1 above. It can be shown to be pairwise distinguishable with respect to L2 as follows.
Let ak and am be arbitrary two different members of the set, where k and m are positive integers and k  m . Select bakb as a string to be appended to ak and am . Then akbakb is in L2 while ambakb is not in L2 . Hence ak and am are distinguishable with respect to L2 . Since ak and am are arbitrary strings of S2, S2 satisfies the conditions of Myhill-Nerode theorem. Hence L2 is nonregular.

https://www.cs.odu.edu/~toida/nerzic/390teched/regular/reg-lang/non-regularity.html

-->

---

### Задача 4

Доказать нерегулярность языка:

`$$L = \{\omega | \omega = x_1 x_2 … x_k, \ \forall \ k >= 0, x_i \in 1^* \lor (x_i \ne x_j, i \ne j)\}$$`

---

### Задача 5

Доказать нерегулярность языка:

`$$L = \{0^n1^m2^n | n,m \in \mathbb{N_0}\}$$`

---

### Задача 6

Доказать нерегулярность языка:

`$$L = \{0^n1^m | n \leq m\}$$`


---

### Задача 7

Доказать выполнение леммы о накачке, но нерегулярность языка:

`$$L = b^* \cup \{ ab^k | k \in \text{PRIMES} \} \cup aa^+b^\ast$$`
  